var express = require("express");
var bodyParser = require('body-parser');
var app = express();
var fs = require('fs');
var path = require('path');
var appConfig = require('./conf/appConfig.js');
// Set global Express headers
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
// Enable bodyParser
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}));
// Serve static files from angular app dist folder
// Add all routes
fs.readdirSync(path.join(__dirname, 'routes')).forEach(function (file) {
    if (file[0] === '.') {
        return;
    }
    require(path.join(__dirname, 'routes', file))(app);
});
app.use(function (req, res, next) {
        req.setTimeout(0) // no timeout for all requests, your server will be DoS'd
        next()
    })
    // Start listening
app.listen(80);