var mysql = require('mysql');
var Promise = require('promise')
appConfig = require('../conf/appConfig.js');
// Initialise database connection
var pool = mysql.createPool({
    connectionLimit: 100
    , waitForConnections: true
    , queueLimit: 0
    , host: appConfig.db_config.host
    , port: appConfig.db_config.port
    , user: appConfig.db_config.user
    , password: appConfig.db_config.password
    , database: appConfig.db_config.database
    , debug: true
    , wait_timeout: 28800
    , connect_timeout: 10
    , insecureAuth: true
});
exports.getData = function (queryString) {
    return new Promise(function (resolve, reject) {
        pool.query(queryString, function (err, data, fields) {
            if (err) {
                if (err.code === 'PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR') {
                    for (i = 0; i < 10; i++) {
                        pool.query(queryString, function (err, data, fields) {
                            if (err) {
                                if (err.code === 'PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR') {
                                    console.log('Trying mysql connection again')
                                }
                            }
                            else {
                                resolve(data)
                            }
                        })
                    }
                    console.log(err)
                    resolve(err)
                }
            }
            else {
                resolve(data)
            }
        })
    })
}
exports.insert = function (queryString, dataToInsert) {
    return new Promise(function (resolve, reject) {
        pool.query(queryString, dataToInsert, function (err, results, fields) {
            if (err) {
                if (err.code === 'PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR') {
                    for (i = 0; i < 10; i++) {
                        pool.query(queryString, dataToInsert, function (err, results, fields) {
                            if (err) {
                                if (err.code === 'PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR') {
                                    console.log('Trying mysql connection again')
                                }
                            }
                            else {
                                resolve(results)
                            }
                        })
                    }
                }
                console.log(err);
                resolve(err)
            }
            else {
                resolve(results)
            }
        });
    })
}
exports.update = function (queryString, dataToUpdate) {
    return new Promise(function (resolve, reject) {
        pool.query(queryString, dataToUpdate, function (err, results, fields) {
            if (err) {
                if (err.code === 'PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR') {
                    for (i = 0; i < 10; i++) {
                        pool.query(queryString, dataToUpdate, function (err, results, fields) {
                            if (err) {
                                if (err.code === 'PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR') {
                                    console.log('Trying mysql connection again')
                                }
                            }
                            else {
                                resolve(results)
                            }
                        })
                    }
                }
                console.log(err);
                resolve(err)
            }
            else {
                resolve(results)
            }
        });
    })
}