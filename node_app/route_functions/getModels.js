var dbHelper = require('../utils/databaseHelper.js')
var Promise = require('promise');

exports.function = function() {
    return new Promise(function(resolve, reject) {
        var getData = dbHelper.getData("SELECT * FROM cloud_modeller_builder.models WHERE valid_to is null")
        getData.then(function(result) {
            resolve(result)
        })
    })
}