var dbHelper = require('../utils/databaseHelper.js')
var Promise = require('promise');

exports.function = function(req) {
  return new Promise(function(resolve, reject) {
    var versionDates = req.query.version.split('/');
    dbHelper.getData('SELECT * FROM models WHERE valid_to is NULL AND id = ' + req.query.id).then(function(modelsSelectResult) {
      if (typeof modelsSelectResult.errno == 'undefined') {
        delete modelsSelectResult[0].db_id
        delete modelsSelectResult[0].id
        delete modelsSelectResult[0].valid_from
        delete modelsSelectResult[0].valid_to
        delete modelsSelectResult[0].updated_date
        delete modelsSelectResult[0].deployed
        dbHelper.insert('INSERT INTO models SET ?', modelsSelectResult).then(function(modelsInsertResult) {
          dbHelper.getData('SELECT models.deployed, model_components.db_id, model_components.id, model_components.name, model_components.x, model_components.y, model_components.component_spec, lu_components.name AS type, lu_components.id AS type_id FROM model_components JOIN lu_components ON lu_components.id = model_components.component_id JOIN models ON models.id = model_components.model_id WHERE model_components.model_id =' + req.query.id + ' AND models.valid_to = "' + versionDates[0] + '" AND model_components.valid_to IS NULL AND model_components.valid_from <= "' + versionDates[0] + '" OR model_components.valid_from <= "' + versionDates[0] + '" AND model_components.valid_to > "' + versionDates[0] + '" AND models.valid_to = "' + versionDates[0] + '"').then(function(componentSelectResult) {
            if (typeof componentSelectResult.errno == 'undefined') {
              for (item in componentSelectResult) {
                componentSelectResult[item].restored = true;
                componentSelectResult[item].model_id = modelsInsertResult.insertId;
		        delete componentSelectResult[item].deployed
		        delete componentSelectResult[item].type
		        delete componentSelectResult[item].type_id
		        delete componentSelectResult[item].id
		        delete componentSelectResult[item].db_id
              }
              dbHelper.insert('INSERT INTO model_components SET ?', componentSelectResult).then(function(componentInsertResult) {
              	resolve(componentInsertResult)
              })
            } else {
              resolve('Error selecting model data')
            }
          })
        })
      } else {
        resolve('Error selecting model data')
      }
    })
  })
}