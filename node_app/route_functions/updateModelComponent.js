var dbHelper = require('../utils/databaseHelper.js')
var Promise = require('promise');
exports.function = function(req) {
    return new Promise(function(resolve, reject) {

        dbHelper.insert('UPDATE model_components SET valid_to = CURRENT_TIMESTAMP() WHERE valid_to is null AND id = ' + req.query.id).then(function(updateModelComponentResponse) {
            if (typeof updateModelComponentResponse.errno == 'undefined') {
                req.body.id=req.query.id
                dbHelper.insert('INSERT INTO model_components SET ?', req.body).then(function(updateModelComponentResponse) {
                    if (typeof updateModelComponentResponse.errno == 'undefined') {
                        resolve('done')
                    } else {
                        resolve('Error updating model component')
                    }
                })
            } else {
                //Error Log
                resolve('Error updating model component')
            }
        })

    })
}