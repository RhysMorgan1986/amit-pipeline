var dbHelper = require('../utils/databaseHelper.js')
var Promise = require('promise');
var imageData

exports.function = function(req) {
    return new Promise(function(resolve, reject) {
            dbHelper.getData("SELECT distinct(image_name), lu_images.image_id FROM cloud_modeller_component.consumables JOIN cloud_modeller_component.consumable_image_mapping ON cloud_modeller_component.consumable_image_mapping.consumable_id = cloud_modeller_component.consumables.id JOIN cloud_modeller_component.lu_images ON cloud_modeller_component.lu_images.id = cloud_modeller_component.consumable_image_mapping.image_id WHERE cloud_modeller_component.consumables.cpu = " + req.cpu + " AND cloud_modeller_component.consumables.mem = " + req.mem + " and cloud_modeller_component.consumables.type = '" + req.type + "'").then(function(imageResults) {
                if (typeof imageResults.errno == 'undefined') {
                    imageData = {
                        "name": "image",
                        "display": "Image",
                        "type": "select",
                        "values": []
                    }
                    for (i in imageResults) {
                        imageData.values.push({
                            "value": imageResults[i].image_id,
                            "display": imageResults[i].image_name,
                            "subitems": []
                        })
                    }
                    resolve(imageResults)
                } else {
                    //Error Log
                    resolve('Error retrieving images')
                }
            })
    })
}