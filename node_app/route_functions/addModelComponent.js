var dbHelper = require('../utils/databaseHelper.js')
var Promise = require('promise');
exports.function = function (req) {
    return new Promise(function (resolve, reject) {
        dbHelper.insert('insert into model_components set ?', req.body).then(function (addModelComponentResponse) {
            if (typeof addModelComponentResponse.errno == 'undefined') {
                resolve('Success')
            }
            else {
                //Error Log
                resolve('Error adding model component')
            }
        })
    })
}