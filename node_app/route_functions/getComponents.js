var dbHelper = require('../utils/databaseHelper.js')
var Promise = require('promise');

exports.function = function() {
    return new Promise(function(resolve, reject) {
        dbHelper.getData('select id,name,img,component_options from lu_components;').then(function(componentResults) {
            if (typeof componentResults.errno == 'undefined') {
                resolve(componentResults)
            } else {
                //Error Log
                resolve('Error retrieving components')
            }
        })
    })
}