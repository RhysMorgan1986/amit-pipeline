var dbHelper = require('../utils/databaseHelper.js')
var Promise = require('promise');
exports.function = function (req) {
    return new Promise(function (resolve, reject) {
        dbHelper.getData('UPDATE model_components SET deleted_date = CURRENT_TIMESTAMP WHERE valid_to is null and id=' + req.query.id).then(function (deleteModelComponentResult) {
            if (typeof deleteModelComponentResult.errno == 'undefined') {
                resolve('Success')
            }
            else {
                //Error Log
                resolve('Error deleting component')
                
            }
        })
    })
}
