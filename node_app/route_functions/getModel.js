var dbHelper = require('../utils/databaseHelper.js')
var Promise = require('promise');
var request = require('request')
var cloud_modeller_state_svc = 'http://state-svc-ms/api'
var cloud_modeller_price_svc = 'http://price-svc-ms/api'
exports.function = function(req) {
  return new Promise(function(resolve, reject) {
    var modelQuery;
    if (req.query.version == 'latest') {
      modelQuery = ' AND model_components.valid_to is NULL AND models.valid_to is NULL;'
    } else {
      var versionDates = req.query.version.split('/');
      modelQuery = ' AND models.valid_to = "'+versionDates[1]+'" AND model_components.valid_to IS NULL AND model_components.valid_from <= "'+versionDates[0]+'" OR model_components.valid_from <= "'+versionDates[0]+'" AND model_components.valid_to >= "'+versionDates[0]+'" AND models.valid_to = "'+versionDates[0]+'"'
    }

    dbHelper.getData('SELECT models.deployed, model_components.db_id, model_components.deleted_date, model_components.id, model_components.name, model_components.x, model_components.y, model_components.component_spec, lu_components.name AS type, lu_components.id AS type_id FROM model_components JOIN lu_components ON lu_components.id = model_components.component_id JOIN models ON models.id = model_components.model_id WHERE model_components.model_id =' + req.query.id + modelQuery).then(function(modelData) {
      if (typeof modelData.errno == 'undefined') {
        if (modelData.length == 0) {
          resolve([])
        }
        if (modelData[0].deployed !== null) {
          var statePromises = []
          for (modelComp in modelData) {
            var statePromise = new Promise(function(resolve, reject) {
              var intComp = modelComp;
              getState(modelData[intComp].id).then(function(stateResult) {
                modelData[intComp].state = stateResult;
                resolve('done')
              })
            })
            statePromises.push(statePromise)
          }
          Promise.all(statePromises).then(function(stateResults) {
            console.log('hit')
            var modelPrice = 0
            var pricePromises = []
            for (modelComp in modelData) {
              var pricePromise = new Promise(function(resolve, reject) {
                var intComp = modelComp;
                getPrice(modelData[intComp]).then(function(priceResult) {
                  modelData[intComp].cost = priceResult;
                  modelPrice += parseInt(priceResult.hour)
                  resolve('done')
                })
              })
              pricePromises.push(pricePromise)
            }
            Promise.all(pricePromises).then(function(priceResults) {
              console.log('hit')
              resolve(modelData)
            })
          })
        } else {
          var modelPrice = 0
          var pricePromises = []
          for (modelComp in modelData) {
            var pricePromise = new Promise(function(resolve, reject) {
              var intComp = modelComp;
              getPrice(modelData[intComp]).then(function(priceResult) {
                modelData[intComp].cost = priceResult;
                modelPrice += parseInt(priceResult.hour)
                resolve('done')
              })
            })
            pricePromises.push(pricePromise)
          }
          Promise.all(pricePromises).then(function(priceResults) {
            resolve(modelData)
          })
        }
      } else {
        //Error Log
        resolve('Error retrieving model data')
      }
    })
  })
}

function getState(compId) {
  return new Promise(function(resolve, reject) {
    var intCompId = compId
    request({
      headers: {
        'Content-Type': 'application/json',
      },
      uri: cloud_modeller_state_svc + '/readState?by=componentId&id=' + intCompId,
      method: 'GET'
    }, function(err, res, resbody) {
      if (!err) {
        var state = JSON.parse(resbody)
        resolve(state);
      } else {
        console.log(err)
        resolve('Error: Failed to get state.')
      }
    });
  })
}

function getPrice(modelComp) {
  return new Promise(function(resolve, reject) {
    console.log(modelComp.component_spec)
    var intmodelComp = modelComp
    var compSpec = JSON.parse(intmodelComp.component_spec)
    var spec = {
      "type": intmodelComp.type,
      "type_id": intmodelComp.type_id,
      "cpu": compSpec.cpu,
      "mem": compSpec.memory,
      "os": compSpec.os
    }
    console.log('price')
    request({
      headers: {
        'Content-Type': 'application/json',
      },
      uri: cloud_modeller_price_svc + '/priceGenerator',
      method: 'POST',
      body: JSON.stringify(spec)
    }, function(err, res, resbody) {
      console.log('resbody')
      console.log(resbody)
      var price = JSON.parse(resbody)
      resolve(price.cost);
    });
  })
}