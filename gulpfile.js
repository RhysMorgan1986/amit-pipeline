var gulp = require('gulp')
var mocha = require('gulp-mocha');
var fs = require('fs')
var template = require('gulp-template');
var rename = require('gulp-rename')
    // Build Tasks
    // Test Tasks
gulp.task('test', function () {
    return gulp.src(['node_app/tests/*.spec.js'], {
        read: false
    }).pipe(mocha({
        "reporter": "mocha-jenkins-reporter"
        , "reporterOptions": {
            "junit_report_name": "Tests"
            , "junit_report_path": "test_results/mocha_report.xml"
            , "junit_report_stack": 1
        }
        , globals: {
            should: require('should')
        }
        , timeout: 60000
    }));
});
// Helpers
gulp.task('route', function () {
    fs.access('node_app/routes/' + process.argv[4] + '.js', function (err) {
        if (err) {
            // file/path is not visible to the calling process
            gulp.src('helper_templates/route_function.js').pipe(template({
                name: process.argv[4]
            })).pipe(rename(process.argv[4] + '.js')).pipe(gulp.dest('node_app/route_functions/'))
            gulp.src('helper_templates/route.js').pipe(template({
                name: process.argv[4]
            })).pipe(rename(process.argv[4] + '.js')).pipe(gulp.dest('node_app/routes/'))
            gulp.src('helper_templates/route_function_test.js').pipe(template({
                name: process.argv[4]
            })).pipe(rename(process.argv[4] + '.spec.js')).pipe(gulp.dest('node_app/tests/'))
        }
        else {
            console.log('ERROR: Route with the name: ' + process.argv[4] + '.js already exists in this project')
        }
    });
})